FROM docker.io/alpine as builder

# Redis Exporter image for OpenShift Origin

ARG RDXPVERS=1.27.0
ENV RDXPURL=https://github.com/oliver006/redis_exporter/releases/download

RUN set -x \
    && if test `uname -m` = aarch64; then \
	ARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	ARCH=arm; \
    elif test `uname -m` = armv6l; then \
	ARCH=arm; \
    else \
	ARCH=amd64; \
    fi \
    && wget -O /tmp/rdxp.tar.gz \
	"$RDXPURL/v$RDXPVERS/redis_exporter-v$RDXPVERS.linux-$ARCH.tar.gz" \
    && tar -C /tmp -xf /tmp/rdxp.tar.gz --strip-components=1 \
    && chmod +x /tmp/redis_exporter \
    && apk --no-cache add ca-certificates

FROM scratch

ARG RDXPVERS=1.27.0

LABEL io.k8s.description="Redis Prometheus Exporter Image." \
      io.k8s.display-name="Redis Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,redis" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-redisexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$RDXPVERS"

COPY --from=builder /tmp/redis_exporter /redis_exporter
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

USER 1001
ENTRYPOINT [ "/redis_exporter" ]
