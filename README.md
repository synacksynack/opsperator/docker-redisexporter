# k8s Prometheus Redis Exporter

Based on github.com/oliver006/redis_exporter

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```
